# История изменений

## v0.3.4, 29.08.2019

Изменение в настройках главной страницы. Добавлена возможность создавать несколько страниц, которые можно вызывать через ?page=ИМЯ_СТРАНИЦЫ. Каталог для размещения страниц /custom/dashboard/. По умолчанию загружается index.php. Если он отсутствует, то index.sample.php (пример файла).
Для приведения дерева каталогов в актуальное состояние необходимо перенести и переименовать файл custom/dashboard.php в custom/dashboard/index.php. 

## v0.3.3, 26.08.2019

Поля минимальных и максимальных значений величин для измерительных приборов перенесены в единицы измерения. Значения используются при отрисовке графиков для устранения визуальных резких скачков при небольших изменениях величины.
```sql
ALTER TABLE meter_units ADD COLUMN minimal float;
ALTER TABLE meter_units ADD COLUMN maximal float;
ALTER TABLE meters DROP COLUMN minimal;
ALTER TABLE meters DROP COLUMN maximal;
```

## v0.3.2, 25.08.2019

Обновлён файл phpmd.service для systemd. В том числе, изменено местоположение файлов журналов, pid-файлов для демонов и кеша. Необходимые папки /var/log/phpmd, /var/run/phpmd и /var/cache/phpmd создаются при запустке демонов через systemd. При использовании других систем инициализации данные папки нужно создать вручную.

Папки daemon/log и daemon/pid могут быть удалены.

## v0.3.1, 22.08.2019

### Изменение базы данных

Комментарии к таблицам вынесены из комментариев SQL в комментарии к таблицам. Удалена устаревшая таблица auth_codes. Для приведения базы данных к текущему виду выполните следующие запросы:
```sql
DROP TABLE auth_codes;
COMMENT ON TABLE auth_users IS 'Пользователи системы';
COMMENT ON TABLE auth_server IS 'Клиенты сервера OAuth 2.0';
COMMENT ON TABLE auth_tokens IS 'Токены OAuth 2.0';
COMMENT ON TABLE auth_sessions IS 'Сессии пользователей системы';
COMMENT ON TABLE auth_fail2ban IS 'Данные об ошибках аутентификации';
COMMENT ON TABLE places IS 'Места размещения устройств';
COMMENT ON TABLE modules IS 'Модули';
COMMENT ON TABLE devices IS 'Устройства';
COMMENT ON TABLE meter_units IS 'Типы измеряемых параметров';
COMMENT ON TABLE meters IS 'Измерительные датчики';
COMMENT ON TABLE meter_history IS 'Журнал измерений';
COMMENT ON TABLE indicators IS 'Контрольные датчики';
COMMENT ON TABLE indicator_history IS 'Журнал контрольных датчиков';
COMMENT ON TABLE variables IS 'Пользовательские и системные переменные';
COMMENT ON TABLE tts_log IS 'Журнал голосовых сообщений';
```

## v0.3.0, 19.08.2019

### Изменение базы данных

Добавлена таблица tts_log - лог голосовых сообщений. Для приведения БД к требуемому виду выполните запрос:
```sql
CREATE TABLE tts_log (
    id bigint PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY,
    timestamp timestamptz DEFAULT CURRENT_TIMESTAMP,
    text text
);
CREATE INDEX tts_log_timestamp_idx ON tts_log (timestamp);
```

## v0.2.0

До данной версии история изменений не оформлялась.